# Change Log
All notable changes to this project will be documented in this file.
 
The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [0.2] - 2022-07-17

### Added
- One-page site was prepared to demonstrate the work of the model
- Separate python environment was created for the project. All dependencies are in requirements.txt

### Changed
- The code for converting responses to lowercase was taken out of the distractor generation function. All transformation of input responses is in dg_site.py file

### Fixed 
- Extra lines of code have been removed from the distractor generation function

## [0.1] - 2022-07-10

### Added

- SciQ dataset was analyzed
- A search was made for a suitable model for solving the problem
- For the solution, the GPT-2 124M model with transfer learning was selected
 
