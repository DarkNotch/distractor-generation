# Distractor Generation

## Description
The developed one-page site allows users to generate distractors - wrong answers to questions that look like the right ones. This development can be used by test writers to minimize the time for inventing incorrect answers for questions.

At the moment, the generator can work for questions from the area:
1. Biology
2. Physics
3. Astronomy

## Dependencies
This program uses the following libraries:
1. [Keras](https://keras.io/)
2. [TensorFlow](https://www.tensorflow.org/)
3. [Flask](https://flask.palletsprojects.com/en/2.1.x/)

## Installation
First you should clone the repository on your computer: 
```
git clone https://gitlab.com/DarkNotch/distractor-generation
```
Then go to there.

Server deployment can be completed in two ways:
1. Via docker-compose:
 - install docker-compose and run:
```
docker-compose up
```
2. Manually:
 - Go to src folder and install dependencies:
```
pip install --no-cache-dir -r requirements.txt
```
 - Start the server:
```
python dg_site.py
```
After that, a server on port 8080 will be launched on your computer. The site is available from the main page.

## Usage
Enter your question, if you wish, all possible answers and select the number of distractors. After pressing the get distractors button on the website, after some time, the number of incorrect answers chosen by you will be generated, none of which will match the entered correct answers.

## License
See [LICENSE](LICENSE)
