import numpy as np
from keras_gpt_2 import load_trained_model_from_checkpoint, get_bpe_from_files

CONFIG_FILEE = 'hparams.json'
CHECKPOINT_FILE = 'model-3000'
ENCODER_FILE = 'encoder.json'
VOCAB_FILE = 'vocab.bpe'

NUM_DISTRACTORS_DEFAULT = 3
END_CHAR_DEFAULT = '\n'
MAX_LEN_DEFAULT = 20


class DistractorGenerator:
    def __init__(self, model_folder):
        self.model_folder = model_folder
        self.model = load_trained_model_from_checkpoint(
            self.model_folder + CONFIG_FILEE,
            self.model_folder + CHECKPOINT_FILE
        )
        self.bpe = get_bpe_from_files(
            self.model_folder + ENCODER_FILE,
            self.model_folder + VOCAB_FILE
        )

    def generate(self, question, num_distractors=NUM_DISTRACTORS_DEFAULT, correct_answers=None,
                 end_char=END_CHAR_DEFAULT, max_len=MAX_LEN_DEFAULT):
        question = f'Question: {question} Distractor:'

        distractors = []
        i = 0

        while True:
            n = 0
            distractor = ''

            pred = self.model.predict([self.bpe.encode(question)], verbose=0)[0, -1].argsort()[-(i + 1)::][::-1][-1]
            decoded_pred = self.bpe.decode([pred])

            while decoded_pred != end_char:
                n += 1
                if n > max_len:
                    print(f'WARNING: Can\'t find end character in distractor #{i + 1}, it\'ll be removed')
                    break

                distractor += decoded_pred.lower()
                pred = np.argmax(self.model.predict([self.bpe.encode(question + distractor)])[0, -1])
                decoded_pred = self.bpe.decode([pred])

            if distractor in distractors:
                num_distractors += 1
            elif correct_answers is not None and distractor.strip().lower() in correct_answers:
                num_distractors += 1
            else:
                distractors.append(distractor)

            i += 1
            if i >= num_distractors:
                break

        distractors = list(map(lambda x: x.strip().capitalize(), distractors))
        return distractors
