from flask import Flask, render_template, request
from generator import DistractorGenerator, NUM_DISTRACTORS_DEFAULT

MODEL_FOLDER = '../models/124M_trained/'

app = Flask(__name__)
generator = DistractorGenerator(MODEL_FOLDER)


@app.route('/', methods=['GET', 'POST'])
def index(distractors=None):
    if request.method == 'POST':
        question = get_question(request.form)
        answers = get_answers(request.form)
        num_distractors = get_num_distractors(request.form)

        distractors = generator.generate(question, num_distractors, answers)
    return render_template('index.html', distractors=distractors)


def get_question(form):
    question = form['question']
    if question[-1] != '?':
        question = question[:-1] + '?'
    return question


def get_answers(form):
    answers = form['answers'].split(',')
    new_answers = []

    for answer in answers:
        if answer.strip():
            new_answers.append(answer.strip().lower())
    return new_answers


def get_num_distractors(form):
    try:
        num_distractors = int(form['num_distractors'])
        return num_distractors
    except TypeError:
        return NUM_DISTRACTORS_DEFAULT


if __name__ == '__main__':
    app.run(debug=True, host="0.0.0.0", port=8080)
